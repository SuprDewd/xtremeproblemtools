# Xtreme Problem Tools

These are tools to manage problem packages using the Xtreme problem package
format.


## Programs Provided

The problem tools provide the following three programs:

 - `xtreme verify`: run a complete check on a problem
 - `xtreme pdf`: convert a problem statement to pdf
 - `xtreme html`: convert a problem statement to html

Running any of them without any command-line options gives
documentation on what parameters they accept.


## Installing and using problemtools

In order for the tools to work, you first have to compile the various
support programs, which can be done by running `make` in the root
directory of problemtools.

The checktestdata program requires a relatively recent gcc version
(4.8 suffices), but is only needed for running checktestdata input
validation scripts.  The rest of problemtools will run fine without
it, but in this case you need to build the other programs separately,
e.g. by running

    (cd support/default_validator && make)
    (cd support/interactive && make)

When this is done, you can run the program `xtreme` the src directory of
problemtools.


## Requirements and compatibility

To run the tools, you need Python 2 with the YAML and PlasTeX libraries,
and a LaTeX installation.  In Ubuntu, the precise dependencies are as follows:

    libboost-regex1.54.0, libc6 (>= 2.14), libgcc1 (>= 1:4.1.1), libgmp10, libgmpxx4ldbl, libstdc++6 (>= 4.4.0), python (>= 2.7), python (<< 2.8), python:any (>= 2.7.1-0ubuntu2), python-yaml, python-plastex, texlive-latex-recommended, texlive-fonts-recommended, texlive-latex-extra, texlive-lang-cyrillic, tidy, ghostscript

The problem tools have not been tested on other platforms.  If you do
test on another platform, we'd be happy to hear what worked and what
did not work, so that we can write proper instructions (and try to
figure out how to make the non-working stuff work).
