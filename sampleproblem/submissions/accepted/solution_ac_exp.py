import sys
import itertools

MOD = 1000000007

class Matrix:
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.arr = [ [ 0 for j in range(cols) ] for i in range(rows) ]

    def __getitem__(self, idx):
        i, j = idx
        return self.arr[i][j]

    def __setitem__(self, idx, val):
        i, j = idx
        self.arr[i][j] = val

    def __mul__(self, other):
        assert self.cols == other.rows
        res = Matrix(self.rows, other.cols)
        for k in range(self.cols):
            for i in range(self.rows):
                for j in range(other.cols):
                    res.arr[i][j] += self.arr[i][k] * other.arr[k][j]
                    res.arr[i][j] %= MOD
        return res

    def __pow__(self, p):
        assert self.rows == self.cols
        base = self
        res = Matrix(self.rows, self.rows)
        for i in range(self.rows):
            res[i,i] = 1

        while p > 0:
            if (p & 1) != 0:
                res = res * base
            base = base * base
            p >>= 1

        return res

K,N = map(int, sys.stdin.readline().strip().split())

objects = []
for i in range(N):
    lane, unit = map(int, sys.stdin.readline().strip().split())
    lane -= 1
    unit -= 1
    objects.append((lane, unit))

compact = []
for key, grp in itertools.groupby(sorted(objects, key=lambda x: x[1]), key=lambda x: x[1]):
    compact.append((key, set([ ln for ln, _ in grp ])))

compact.append((K-1,set()))

trans = Matrix(4,4)
for i in range(4):
    if 0 < i:
        trans[i,i-1] = 1
    trans[i,i] = 1
    if i < 3:
        trans[i,i+1] = 1

res = Matrix(1, 4)
res[0,0] = 1
last = 0
for at, obj in compact:
    res = res * trans**(at - last)
    for bad in obj:
        res[0,bad] = 0
    last = at

print(res[0,0] % MOD)

