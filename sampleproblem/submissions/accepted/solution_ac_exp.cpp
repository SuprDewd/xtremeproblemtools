#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

long long mod = 1000000007;

class matrix {
public:
    int rows, cols;
    matrix(int r, int c) : rows(r), cols(c), cnt(r * c) {
        data.assign(cnt, 0); }
    matrix(const matrix& other) : rows(other.rows), cols(other.cols),
        cnt(other.cnt), data(other.data) { }
    long long& operator()(int i, int j) { return at(i, j); }
    matrix operator *(const matrix& other) {
        matrix res(rows, other.cols);
        for (int i = 0; i < rows; i++) for (int j = 0; j < other.cols; j++)
            for (int k = 0; k < cols; k++) {
                res(i, j) += at(i, k) * other.data[k * other.cols + j] % mod;
                if (res(i, j) >= mod)
                    res(i,j) -= mod;
            }
        return res; }
    matrix pow(long long p) {
        matrix res(rows, cols), sq(*this);
        for (int i = 0; i < rows; i++) res(i, i) = 1;
        while (p) {
            if (p & 1) res = res * sq;
            p >>= 1;
            if (p) sq = sq * sq;
        } return res; }
private:
    int cnt;
    vector<long long> data;
    inline long long& at(int i, int j) { return data[i * cols + j]; }
};

int main() {
    long long K;
    int n;
    cin >> K >> n;

    vector<pair<long long, int> > objs;
    for (int i = 0; i < n; i++) {
        int lane;
        long long unit;
        cin >> lane >> unit;
        lane--, unit--;
        objs.push_back(make_pair(unit, lane));
    }

    sort(objs.begin(), objs.end());

    matrix trans(4,4);
    for (int i = 0; i < 4; i++) {
        if (0 < i) trans(i,i-1) = 1;
        trans(i,i) = 1;
        if (i < 3) trans(i,i+1) = 1;
    }

    matrix res(1, 4);
    res(0,0) = 1;
    long long last = 0;
    for (int i = 0; i < n; ) {
        int start = i;
        matrix cur(4,4);
        for (int j = 0; j < 4; j++)
            cur(j,j) = 1;
        while (i < n && objs[i].first == objs[start].first) {
            cur(objs[i].second,objs[i].second) = 0;
            i++;
        }
        res = res * trans.pow(objs[start].first - last) * cur;
        last = objs[start].first;
    }

    res = res * trans.pow(K-1 - last);
    cout << res(0,0) << endl;

    return 0;
}

