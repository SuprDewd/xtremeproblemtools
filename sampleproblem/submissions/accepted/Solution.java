import java.io.*;
import java.util.*;

public class Solution {
	long highwayLen;
	HashMap<Long,Gate> gates = new HashMap<Long,Gate>();

	static final int NUM_LANES = 4;
	static final long ANSWER_MODULO = 1000000007;
	//static final long ANSWER_MODULO = 1000000000000000000l;

	public static void main(String[] args) {

		preComputeMatrices();

		Scanner in = new Scanner(System.in);
		Solution me = new Solution();

		me.highwayLen = in.nextLong();
		int numCows = in.nextInt();

		for (int lane = 2; lane <= NUM_LANES; lane++) {
			me.closeLane(1, lane);
			me.closeLane(me.highwayLen, lane);
		}

		for (int i = 0; i < numCows; i++) {
			int lane = in.nextInt();
			long highwayUnit = in.nextLong();
			me.closeLane(highwayUnit, lane);
		}

		me.solve();
	}

	/**
	 * Contains transformation matrices, where the ith matrix
	 * describes the number of ways to get from any lane in
	 * the highway to any other lane at a distance 2^i units
	 * away. We use matrix multiplication to precompute distances
	 * of 1, 2, 4, 8, 16, ..., 2^59. Then, when it's time to
	 * travel along, we can very quickly calculate the
	 * transformation for any distance from 1 to 2^60.
	 */
	static Matrix [] stepMatrices;
	static final Matrix ONE_STEP = new Matrix(new long[][] {
				{ 1, 1, 0, 0 },
				{ 1, 1, 1, 0 },
				{ 0, 1, 1, 1 },
				{ 0, 0, 1, 1 }
				});
	static void preComputeMatrices()
	{
		stepMatrices = new Matrix[60];
		stepMatrices[0] = ONE_STEP;
		for (int i = 1; i < 60; i++) {
			stepMatrices[i] = stepMatrices[i-1].multiply(stepMatrices[i-1]);
		}
	}

	/**
	 * Calculates the transformation matrix for any distance
	 * along the highway. Uses the precalculated stepMatrices
	 * to quickly calculate the desired matrix.
	 */
	Matrix getWaysMatrix(long steps)
	{
		// this is quick log-base-2 calculation
		int powr = 63 - Long.numberOfLeadingZeros(steps);
		long remaining = steps - (1l << powr);
		if (remaining == 0) {
			// an exact power-of-two is requested
			return stepMatrices[powr];
		}
		else {
			// determine result by multiplying the
			// nearest power-of-two with the remainder...
			// i.e. M^(a+b) = (M^a)(M^b)
			return stepMatrices[powr].multiply(getWaysMatrix(remaining));
		}
	}

	void solve()
	{
		long curDistance = 1;
		long [] ways = new long[4];
		ways[0] = 1;

		Gate [] gg = getGatesInOrder();
		for (int i = 1; i < gg.length; i++) {
			Matrix M = getWaysMatrix(gg[i].highwayUnit - curDistance);
			ways = M.applyTo(ways);
			curDistance = gg[i].highwayUnit;

			// if there are any cows on the road at this
			// distance, eliminate ways to the blocked
			// lanes.
			ways[0] = gg[i].lanes[0] ? 0 : ways[0];
			ways[1] = gg[i].lanes[1] ? 0 : ways[1];
			ways[2] = gg[i].lanes[2] ? 0 : ways[2];
			ways[3] = gg[i].lanes[3] ? 0 : ways[3];
		}

		System.out.println(ways[0]);
	}

	static class Gate
	{
		long highwayUnit;
		/// Indicates lanes that are blocked
		boolean [] lanes;

		Gate(long highwayUnit) {
			this.highwayUnit = highwayUnit;
			this.lanes = new boolean[NUM_LANES];
		}
	}

	static final Gate NULL_GATE = new Gate(0);

	void closeLane(long highwayUnit, int laneNumber)
	{
		createGate(highwayUnit).lanes[laneNumber-1] = true;
	}

	Gate createGate(long highwayUnit)
	{
		Gate g = gates.get(highwayUnit);
		if (g == null) {
			g = new Gate(highwayUnit);
			gates.put(highwayUnit, g);
		}
		return g;
	}

	Gate [] getGatesInOrder()
	{
		Gate [] gg = gates.values().toArray(new Gate[0]);
		Arrays.sort(gg, new Comparator<Gate>() {
			public int compare(Gate a, Gate b) {
				return a.highwayUnit > b.highwayUnit ? 1 :
					a.highwayUnit < b.highwayUnit ? -1 : 0;
			}});
		return gg;
	}

	static class Matrix
	{
		final long [][] values;
		final int width;
		final int height;

		Matrix(long [][] values)
		{
			this.values = values;
			this.width = values[0].length;
			this.height = values.length;
		}

		Matrix multiply(Matrix rhs)
		{
			assert this.width == rhs.height;

			long [][] vv = new long[this.height][rhs.width];
			for (int i = 0; i < this.height; i++) {
				for (int j = 0; j < rhs.width; j++) {
					long sum = 0;
					for (int z = 0; z < this.width; z++) {
						sum = (sum + this.values[i][z] * rhs.values[z][j]) % ANSWER_MODULO;
					}
					vv[i][j] = sum;
				}
			}

			return new Matrix(vv);
		}

		long [] applyTo(long [] x)
		{
			assert x.length == this.width;
			assert x.length == this.height;

			long [] rv = new long[this.height];
			for (int i = 0; i < this.height; i++) {
				long sum = 0;
				for (int z = 0; z < this.width; z++) {
					sum = (sum + this.values[i][z] * x[z]) % ANSWER_MODULO;
				}
				rv[i] = sum;
			}

			return rv;
		}
	}
}
