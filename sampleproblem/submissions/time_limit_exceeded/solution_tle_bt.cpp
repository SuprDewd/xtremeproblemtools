#include <iostream>
#include <set>
using namespace std;

set<pair<long long, long long> > objects;
long long K;
long long mod = 1000000007;

long long bt(long long lane, long unit) {
    if (lane < 0 || lane >= 4) {
        return 0;
    } else if (objects.find(make_pair(lane, unit)) != objects.end()) {
        return 0;
    } else if (unit == K - 1) {
        if (lane == 0) {
            return 1;
        } else {
            return 0;
        }
    } else  {
        long long res = 0;
        res += bt(lane - 1, unit + 1);
        res += bt(lane, unit + 1);
        res += bt(lane + 1, unit + 1);
        res %= mod;
        return res;
    }
}

int main() {
    long long N;
    cin >> K >> N;

    for (int i = 0; i < N; i++) {
        long long lane, unit;
        cin >> lane >> unit;
        lane--, unit--;
        objects.insert(make_pair(lane, unit));
    }

    cout << bt(0, 0) << endl;

    return 0;
}

