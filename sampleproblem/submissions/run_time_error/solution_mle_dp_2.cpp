#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
using namespace std;

int mod = 1000000007;

int main() {

    long long K, N;
    cin >> K >> N;

    vector<pair<long long, int> > objects;
    for (int i = 0; i < N; i++) {
        long long lane, unit;
        cin >> lane >> unit;
        lane--, unit--;
        objects.push_back(make_pair(unit, lane));
    }

    sort(objects.begin(), objects.end());

    long long gap = 0;
    vector<pair<long long, int> > compact;
    for (size_t i = 0; i < objects.size(); i++) {
        if (compact.size() > 0 && objects[i].first == compact[compact.size() - 1].first) {
            compact[compact.size() - 1].second |= 1 << objects[i].second;
        } else {
            compact.push_back(make_pair(objects[i].first, 1 << objects[i].second));
        }
    }

    compact.push_back(make_pair(K - 1, 0));

    for (size_t i = 1; i < compact.size(); i++) {
        gap = max(gap, compact[i].first - compact[i-1].first);
    }

    if (compact.size() == 0) {
        gap = K;
    } else {
        gap = max(gap, compact[0].first - 0);
    }

    int *dp[4][4];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            dp[i][j] = new int[gap + 1];
            dp[i][j][0] = 0;
        }

        dp[i][i][0] = 1;
    }

    for (long long len = 1; len <= gap; len++) {
        for (int start = 0; start < 4; start++) {
            for (int end = 0; end < 4; end++) {
                dp[start][end][len] = 0;

                if (0 < start) {
                    dp[start][end][len] += dp[start - 1][end][len - 1];
                    if (dp[start][end][len] >= mod) dp[start][end][len] -= mod;
                }

                dp[start][end][len] += dp[start][end][len - 1];
                if (dp[start][end][len] >= mod) dp[start][end][len] -= mod;

                if (start < 3) {
                    dp[start][end][len] += dp[start + 1][end][len - 1];
                    if (dp[start][end][len] >= mod) dp[start][end][len] -= mod;
                }
            }
        }
    }

    int res[4], tmp[4];
    memset(res, 0, sizeof(res));
    res[0] = 1;

    long long last = 0;
    for (size_t i = 0; i < compact.size(); i++) {
        for (int end = 0; end < 4; end++) {
            tmp[end] = 0;

            if (!(compact[i].second & (1 << end))) {
                for (int start = 0; start < 4; start++) {
                    tmp[end] += ((long long)res[start] * dp[start][end][compact[i].first - last]) % mod;
                    if (tmp[end] >= mod) tmp[end] -= mod;
                }
            }
        }

        last = compact[i].first;
        for (int end = 0; end < 4; end++) {
            res[end] = tmp[end];
        }
    }

    cout << res[0] << endl;

    return 0;
}
