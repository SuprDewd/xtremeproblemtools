#include <iostream>
#include <cstring>
using namespace std;

int mod = 1000000007;

int main() {
    long long K, N;
    cin >> K >> N;

    bool *obj[4];
    int *dp[4];
    for (int i = 0; i < 4; i++) {
        obj[i] = new bool[K];
        memset(obj[i], 0, K);
        dp[i] = new int[K];
        dp[i][K - 1] = 0;
    }

    for (int i = 0; i < N; i++) {
        long long lane, unit;
        cin >> lane >> unit;
        lane--, unit--;
        obj[lane][unit] = true;
    }

    dp[0][K-1] = 1;
    for (long long unit = K - 2; unit >= 0; unit--) {
        for (int lane = 0; lane < 4; lane++) {
            dp[lane][unit] = 0;
            if (!obj[lane][unit]) {
                if (0 < lane) {
                    dp[lane][unit] += dp[lane - 1][unit + 1];
                    if (dp[lane][unit] >= mod) dp[lane][unit] -= mod;
                }

                dp[lane][unit] += dp[lane][unit + 1];
                if (dp[lane][unit] >= mod) dp[lane][unit] -= mod;

                if (lane < 3) {
                    dp[lane][unit] += dp[lane + 1][unit + 1];
                    if (dp[lane][unit] >= mod) dp[lane][unit] -= mod;
                }
            }
        }
    }

    cout << dp[0][0] << endl;

    return 0;
}
