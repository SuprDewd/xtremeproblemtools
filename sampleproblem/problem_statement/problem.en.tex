\problemname{Xtreme Driving}

Alice, one of the IEEEXtreme participants, is on her way to her university to
take part in this year's contest. To get to the university she has to drive on
a \textit{four} lane highway, but as the highway is very long she quickly becomes bored.
She decides to practice for the contest by thinking about some problems related
to the highway she's driving on. She comes up with the following problem:

\begin{quote}
    Let's say that, in a single unit of time, her car, which is of unit-length, can
    perform one of the following three actions:
        \begin{itemize}
            \item Drive one unit forward, staying on the same lane
            \item If the car is not on the left-most lane: drive one unit forward and switch to the lane on the left
            \item If the car is not on the right-most lane: drive one unit forward and switch to the lane on the right
        \end{itemize}

    \begin{figure}[h]
        \centering
        \includegraphics[scale=0.3]{example_0.png}
        \caption{One possible way to drive through a highway of length $K=5$}
    \end{figure}

    If the highway is $K$ units in length, in how many ways is it possible to drive
    through the highway, provided that she starts on the first unit of the highway
    at the left-most lane, and ends at the last unit of the highway, also at the
    left-most lane?
\end{quote}

As she's been training very hard for the contest, it doesn't take her long to
come up with a solution to this problem. But all of a sudden, out of nowhere, a
large cow appears in front of her car, and she just barely manages to avoid
crashing into it. She got a bit too distracted thinking about the problem\ldots
But this incident gives her an idea: what if the highway had a set of
stationary cows that the car must avoid crashing into?

Unfortunately she doesn't have time to think about this version of the problem
as she just arrived at the university and the contest is about to start. When
the contest starts, she is very surprised to see that the problem she was
thinking about is just like one of the problems presented in the contest. What
a coincidence! Again her hard practice pays off and she quickly solves the
problem. But the real question is, can you?

\section*{Input Format}
Input begins with two integers, $K$, the length of the highway, and $N$, the
number of cows standing on the highway, subject to the following constraints:
\begin{itemize}
    \item $2 \leq K \leq 10^{18}$
    \item $0 \leq N \leq 100$
    \item $N \leq 4(K-2)$
\end{itemize}
Then follow $N$ lines. The $i$:th line contains two integers describing the
location of the $i$:th cow, $x_i$, the lane on which the cow is standing ($1$
being the left-most, and $4$ being the right-most), and $y_i$, the unit on
which the cow is standing, subject to the following constraints:
\begin{itemize}
    \item $1 \leq x_i \leq 4$
    \item $1 < y_i < K$
\end{itemize}
Notice that no cows are on the first or last unit of the highway. It is also
guaranteed that no two cows share the same position.

\section*{Output Format}
You are to output the number of ways to drive from the left-most lane at the
first unit of the highway to the left-most lane at the $K$:th unit of the
highway subject to the rules described above, and the additional constraint
that the car must not drive to a position occupied by a cow.

As the number of ways can be quite large, please output the answer modulo
$10^9+7$. Note that this is the same as the remainder when dividing the number
of ways by $10^9+7$.

\section*{Sample}
% This section will be automatically expanded with the first sample test case

\section*{Explanation}
In this example there are three ways to drive through the highway, and they are
shown in the following figures. Notice that the car can drive in between cows
(as long as other driving rules are fulfilled), but it must not drive to a unit
occupied by a cow.
\begin{center}
    \includegraphics[scale=0.3]{example_1.png}
\end{center}
\begin{center}
    \includegraphics[scale=0.3]{example_2.png}
\end{center}
\begin{center}
    \includegraphics[scale=0.3]{example_3.png}
\end{center}

\section*{Editorial}
The following editorial explains an approach for solving this problem. Note
that this is actually not an editorial for Xtreme Driving, but for Block Art.

\subsection*{An Inefficient Approach}

One common approach to this problem was to attempt to store the canvas as a two
dimensional array of integers, in which each cell in the array would be equal
to the number of blocks in each cell on the canvas. This approach mirrors the
figures provided in the *Problem Statement*.

The problem with this approach is that the canvas can be as large as $12$ by
$10^6$. In addition, there can be up to $10^4$ operations that may cover all of
this canvas. The number of times a cell is accessed or changed, therefore,
could be as high as $12 \cdot 10^6 \cdot 10^4 = 1.2 \cdot 10^{11}$, or 120
billion. This approach would certainly result in a time limit exceeded on the
largest test cases.

\subsection*{An Efficient Approach}

Rather than storing the canvas, a better approach is to store two collections
of rectangles, an \texttt{added} collection representing rectangles of blocks
added to canvas, and a \texttt{removed} collection representing rectangles of
blocks removed from the canvas.

Whenever you encounter a query in input, you create a \texttt{sum} variable,
and set it equal to 0. You then iterate through the \texttt{added} collection,
and increase the value in \texttt{sum} by the size of the intersection between
the query rectangle and each rectangle in \texttt{added}. Then, you would
iterate through the \texttt{removed} collection, and reduce the value in
\texttt{sum} by the area of intersection between the query rectangle and each
rectangle in \texttt{removed}. The result of the query is the value in
\texttt{sum}.

Note that in the worst case, the program would create $5000$ rectangles in the
added and removed collections, and then perform $5000$ queries on these
rectangles, resulting in $2.5 \cdot 10^7$ comparisons between rectangles. This
small number of comparisons can easily be completed within the time limits
provided.

