import re
import sys

line = sys.stdin.readline()
assert re.match('^[0-9]+ [0-9]+\n$', line)
K, N = map(int, line.strip().split())
assert 2 <= K <= 10**18
assert 0 <= N <= 100

seen = set()
for i in range(N):
    line = sys.stdin.readline()
    assert re.match('^[0-9]+ [0-9]+\n$', line)
    lane, unit = map(int, line.strip().split())
    assert 1 <= lane <= 4
    assert 2 <= unit <= K-1
    assert (lane,unit) not in seen
    seen.add((lane, unit))

assert sys.stdin.read() == ''
sys.exit(42) # Exit signal 42 means OK
