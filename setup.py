#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup

setup(name='xtreme',
      version='0.1',
      description='Tools for working with the Xtreme Problem Package',
      maintainer='Bjarki Ágúst Guðmundsson',
      maintainer_email='suprdewd@gmail.com',
      packages=['xtreme', 'xtreme.ProblemPlasTeX'],
      install_requires=[
          'Markdown >= 2.6.5',
          'PyYAML >= 3.11',
          'plasTeX >= 1.0',
      ]
)
