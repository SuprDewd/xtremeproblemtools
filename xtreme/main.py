import sys
import os
import argparse
import logging
import re
from optparse import OptionParser

def re_argument(s):
    try:
        r = re.compile(s)
        return r
    except re.error:
        raise ArgumentTypeError('%s is not a valid regex' % s)

def part_argument(s):
    if s not in PROBLEM_PARTS:
        raise ArgumentTypeError("Invalid problem part specified: %s" % s)
    return s

def ap_verify(parser):
    from xtreme.verifyproblem import PROBLEM_PARTS
    parser.add_argument("-s", "--submission_filter", metavar='SUBMISSIONS', help="run only submissions whose name contains this regex.  The name includes category (accepted, wrong_answer, etc), e.g. 'accepted/hello.java' (for a single file submission) or 'wrong_answer/hello' (for a directory submission)", type=re_argument, default=re.compile('.*'))
    parser.add_argument("-d", "--data_filter", metavar='DATA', help="use only data files whose name contains this regex.  The name includes path relative to the data directory but not the extension, e.g. 'sample/hello' for a sample data file", type=re_argument, default=re.compile('.*'))
    parser.add_argument("-t", "--fixed_timelim", help="use this fixed time limit (useful in combination with -d and/or -s when all AC submissions might not be run on all data)", type=int)
    parser.add_argument("-p", "--parts", help="only test the indicated parts of the problem.  Each PROBLEM_PART can be one of %s." % PROBLEM_PARTS, metavar='PROBLEM_PART', type=part_argument, nargs='+', default=PROBLEM_PARTS)
    parser.add_argument("-b", "--bail_on_error", help="bail verification on first error", action='store_true')
    parser.add_argument('problemdir')
    parser.set_defaults(func=do_verify)

def do_verify(args):
    from xtreme.verifyproblem import Problem

    print 'Loading problem %s' % os.path.basename(os.path.realpath(args.problemdir))
    with Problem(args.problemdir) as prob:
        [errors, warnings] = prob.check(args)
        print "%s tested: %d errors, %d warnings" % (prob.shortname, errors, warnings)
    return 0 if errors == 0 else 1

def ap_html(parser):
    from xtreme.problem2html import ConvertOptions, convert
    options = ConvertOptions()
    for (dest, action, short, long, help) in ConvertOptions.available:
        kwargs = {}
        if (action == 'store'):
            kwargs['default'] = options.__dict__[dest]
        parser.add_argument(short, long, dest=dest, help=help, action=action, **kwargs)

    parser.add_argument('problemdir')
    parser.set_defaults(func=do_html)

def do_html(args):
    from xtreme.problem2html import convert
    return 0 if convert(args.problemdir, args) else 1

def ap_pdf(parser):
    from xtreme.problem2pdf import ConvertOptions, convert
    options = ConvertOptions()
    for (dest, action, short, long, help) in ConvertOptions.available:
        kwargs = {}
        if (action == 'store'):
            kwargs['default'] = options.__dict__[dest]
        parser.add_argument(short, long, dest=dest, help=help, action=action, **kwargs)

    parser.add_argument('problemdir')
    parser.set_defaults(func=do_pdf)

def do_pdf(args):
    from xtreme.problem2pdf import convert
    return 0 if convert(args.problemdir, args) else 1

def main(argv):
    parser = argparse.ArgumentParser(description='Xtreme problem format')
    parser.add_argument("-l", "--log-level", dest="loglevel", help="set log level (debug, info, warning, error, critical)", default="warning")
    subparsers = parser.add_subparsers()
    ap_verify(subparsers.add_parser('verify'))
    ap_html(subparsers.add_parser('html'))
    ap_pdf(subparsers.add_parser('pdf'))
    args = parser.parse_args(argv)

    fmt = "%(levelname)s %(message)s"
    logging.basicConfig(stream=sys.stdout,
                        format=fmt,
                        level=eval("logging." + args.loglevel.upper()))
    return args.func(args)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
